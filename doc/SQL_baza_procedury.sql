/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2015-02-02 23:21:28                          */
/* Version :       1.0						                    */
/*==============================================================*/


CREATE DEFINER=`root`@`localhost` PROCEDURE `getProvince`()
BEGIN
	SELECT 
        `wojewodztwa`.`wojewodztwo_nazwa` AS `wojewodztwo_nazwa`
    FROM
        `wojewodztwa`;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `getDistrict`(IN province varchar(30))
BEGIN
	SELECT 
        `powiaty`.`powiat_nazwa` AS `powiat_nazwa`
    FROM
        (`powiaty`
        JOIN `wojewodztwa`)
    WHERE
        ((`wojewodztwa`.`wojewodztwo_nazwa` = province)
            AND (`powiaty`.`wojewodztwo_id` = `wojewodztwa`.`wojewodztwo_id`));
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCommune`(IN province VARCHAR(30), IN district VARCHAR(50))
BEGIN
	SELECT 
        `gminy`.`gmina_nazwa` AS `gmina_nazwa`
    FROM
        ((`gminy`
        JOIN `powiaty`)
        JOIN `wojewodztwa`)
    WHERE
        ((`wojewodztwa`.`wojewodztwo_nazwa` = province)
            AND (`powiaty`.`powiat_nazwa` = district)
            AND (`wojewodztwa`.`wojewodztwo_id` = `powiaty`.`wojewodztwo_id`)
            AND (`powiaty`.`powiat_id` = `gminy`.`powiat_id`));
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `getPrecinct`(IN province VARCHAR(30), IN district VARCHAR(50), IN commune VARCHAR(50))
BEGIN
	SELECT 
        `obreby`.`obreb_nazwa` AS `obreb_nazwa`
    FROM
        (((`obreby`
        JOIN `gminy`)
        JOIN `powiaty`)
        JOIN `wojewodztwa`)
    WHERE
        ((`wojewodztwa`.`wojewodztwo_nazwa` = province)
            AND (`powiaty`.`powiat_nazwa` = district)
            AND (`gminy`.`gmina_nazwa` = commune)
            AND (`wojewodztwa`.`wojewodztwo_id` = `powiaty`.`wojewodztwo_id`)
            AND (`powiaty`.`powiat_id` = `gminy`.`powiat_id`)
            AND (`gminy`.`gmina_id` = `obreby`.`gmina_id`));
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `getLand`(IN province VARCHAR(30), IN district VARCHAR(50), IN commune VARCHAR(50), IN precinct VARCHAR(50))
BEGIN
	SELECT 
        `dzialki`.`dzialka_numer` AS `dzialka_numer`
    FROM
        ((((`dzialki`
        JOIN `obreby`)
        JOIN `gminy`)
        JOIN `powiaty`)
        JOIN `wojewodztwa`)
    WHERE
        ((`wojewodztwa`.`wojewodztwo_id` = province)
            AND (`powiaty`.`powiat_id` = district)
            AND (`gminy`.`gmina_id` = commune)
            AND (`obreby`.`obreb_id` = precinct)
            AND (`wojewodztwa`.`wojewodztwo_id` = `powiaty`.`wojewodztwo_id`)
            AND (`powiaty`.`powiat_id` = `gminy`.`powiat_id`)
            AND (`gminy`.`gmina_id` = `obreby`.`gmina_id`)
            AND (`obreby`.`obreb_id` = `dzialki`.`obreb_id`));
END

CREATE PROCEDURE `getSurface`(IN province VARCHAR(30), IN district VARCHAR(50), IN commune VARCHAR(50), IN precinct VARCHAR(50), IN land VARCHAR(10))
BEGIN
	SELECT 
        `dzialki`.`dzialka_pow_rol` AS `dzialka_pow_rol`,
        `dzialki`.`dzialka_pow_nrol` AS `dzialka_pow_nrol`,
        `dzialki`.`dzialka_pow_peg` AS `dzialka_pow_peg`
    FROM
        ((((`dzialki`
        JOIN `obreby`)
        JOIN `gminy`)
        JOIN `powiaty`)
        JOIN `wojewodztwa`)
    WHERE
        ((`wojewodztwa`.`wojewodztwo_id` = province)
            AND (`powiaty`.`powiat_id` = district)
            AND (`gminy`.`gmina_id` = commune)
            AND (`obreby`.`obreb_id` = precinct)
            AND (`dzialki`.`dzialka_id` = land)
            AND (`wojewodztwa`.`wojewodztwo_id` = `powiaty`.`wojewodztwo_id`)
            AND (`powiaty`.`powiat_id` = `gminy`.`powiat_id`)
            AND (`gminy`.`gmina_id` = `obreby`.`gmina_id`)
            AND (`obreby`.`obreb_id` = `dzialki`.`obreb_id`));
END

USE `farmer_beta`;
DROP procedure IF EXISTS `getSurface`;

DELIMITER $$
USE `farmer_beta`$$
CREATE PROCEDURE `getSurface`(IN province VARCHAR(30), IN district VARCHAR(50), IN commune VARCHAR(50), IN precinct VARCHAR(50), IN land VARCHAR(10))
BEGIN
	SELECT 
        `dzialki`.`dzialka_pow_rol` AS `dzialka_pow_rol`,
        `dzialki`.`dzialka_pow_nrol` AS `dzialka_pow_nrol`,
        `dzialki`.`dzialka_pow_peg` AS `dzialka_pow_peg`
    FROM
        ((((`dzialki`
        JOIN `obreby`)
        JOIN `gminy`)
        JOIN `powiaty`)
        JOIN `wojewodztwa`)
    WHERE
        ((`wojewodztwa`.`wojewodztwo_id` = province)
            AND (`powiaty`.`powiat_id` = district)
            AND (`gminy`.`gmina_id` = commune)
            AND (`obreby`.`obreb_id` = precinct)
            AND (`dzialki`.`dzialka_id` = land)
            AND (`wojewodztwa`.`wojewodztwo_id` = `powiaty`.`wojewodztwo_id`)
            AND (`powiaty`.`powiat_id` = `gminy`.`powiat_id`)
            AND (`gminy`.`gmina_id` = `obreby`.`gmina_id`)
            AND (`obreby`.`obreb_id` = `dzialki`.`obreb_id`));
END$$

DELIMITER ;




USE `farmer_beta`;
DROP procedure IF EXISTS `checkNumber`;

DELIMITER $$
USE `farmer_beta`$$
CREATE PROCEDURE `checkNumber`(IN province VARCHAR(30), IN district VARCHAR(50), IN commune VARCHAR(50), IN precinct VARCHAR(50), IN land VARCHAR(10))
BEGIN
	SELECT 
        COUNT(*)
    FROM
        ((((`dzialki`
        JOIN `obreby`)
        JOIN `gminy`)
        JOIN `powiaty`)
        JOIN `wojewodztwa`)
    WHERE
        ((`wojewodztwa`.`wojewodztwo_id` = province)
            AND (`powiaty`.`powiat_id` = district)
            AND (`gminy`.`gmina_id` = commune)
            AND (`obreby`.`obreb_id` = precinct)
            AND (`dzialki`.`dzialka_id` = land)
            AND (`wojewodztwa`.`wojewodztwo_id` = `powiaty`.`wojewodztwo_id`)
            AND (`powiaty`.`powiat_id` = `gminy`.`powiat_id`)
            AND (`gminy`.`gmina_id` = `obreby`.`gmina_id`)
            AND (`obreby`.`obreb_id` = `dzialki`.`obreb_id`));
END$$

DELIMITER ;



USE `farmer_beta`;
DROP procedure IF EXISTS `getPrecinctId`;

DELIMITER $$
USE `farmer_beta`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getPrecinctId`(IN province VARCHAR(30), IN district VARCHAR(50), IN commune VARCHAR(50), IN precinct VARCHAR(50))
BEGIN
	SELECT 
        `obreby`.`obreb_id` AS `obreb_id`
    FROM
        (((( `obreby`)
        JOIN `gminy`)
        JOIN `powiaty`)
        JOIN `wojewodztwa`)
    WHERE
        ((`wojewodztwa`.`wojewodztwo_id` = province)
            AND (`powiaty`.`powiat_id` = district)
            AND (`gminy`.`gmina_id` = commune)
            AND (`obreby`.`obreb_id` = precinct)
            AND (`wojewodztwa`.`wojewodztwo_id` = `powiaty`.`wojewodztwo_id`)
            AND (`powiaty`.`powiat_id` = `gminy`.`powiat_id`)
            AND (`gminy`.`gmina_id` = `obreby`.`gmina_id`));
END$$

DELIMITER ;



