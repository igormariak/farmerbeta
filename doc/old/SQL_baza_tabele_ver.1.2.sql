/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2015-01-03 13:01:33                          */
/*==============================================================*/

/* == Utworzenie tabel, parametrów i kluczy == */

drop table if exists dzialki_uprawy;

drop table if exists dzialki;

drop table if exists obreby;

drop table if exists gminy;

drop table if exists powiaty;

drop table if exists wojewodztwa;

drop table if exists zabiegi;

drop table if exists zabiegi_rodzaje;

drop table if exists uprawy;

drop table if exists gatunki;

drop table if exists pakiety;

/*==============================================================*/
/* Table: dzialki                                               */
/*==============================================================*/
create table dzialki
(
   dzialki_id           smallint not null auto_increment,
   obreb_id             smallint not null,
   dzialki_numer        char(10) not null,
   dzialki_pow_fiz      decimal(8,4),
   dzialki_pow_rol      decimal(8,4),
   dzialki_pow_nrol     decimal(8,4),
   dzialki_peg          decimal(8,4),
   primary key (dzialki_id)
);

/*==============================================================*/
/* Table: dzialki_uprawy                                        */
/*==============================================================*/
create table dzialki_uprawy
(
   uprawa_id            int not null,
   dzialki_id           smallint not null,
   dzialka_u_powierzchnia decimal(8,4) not null,
   primary key (uprawa_id, dzialki_id)
);

/*==============================================================*/
/* Table: gatunki                                               */
/*==============================================================*/
create table gatunki
(
   gatunek_id           smallint not null auto_increment,
   gatunek_nazwa        char(20) not null,
   primary key (gatunek_id),
   unique index gatunek_nazwa_UNIQUE (gatunek_nazwa asc)
);

/*==============================================================*/
/* Table: gminy                                                 */
/*==============================================================*/
create table gminy
(
   gmina_id             char(7) not null,
   powiat_id            char(4) not null,
   gmina_nazwa          char(50) not null,
   primary key (gmina_id)
);

/*==============================================================*/
/* Table: obreby                                                */
/*==============================================================*/
create table obreby
(
   obreb_id             smallint not null auto_increment,
   gmina_id             char(7) not null,
   obreb_nazwa          char(50) not null,
   obreb_numer          char(4) not null,
   primary key (obreb_id),
   unique index obreb_nazwa_UNIQUE (obreb_nazwa asc),
   unique index obreb_numer_UNIQUE (obreb_numer asc)
);

/*==============================================================*/
/* Table: pakiety                                               */
/*==============================================================*/
create table pakiety
(
   pakiet_id            smallint not null auto_increment,
   pakiet_nazwa         char(10) not null,
   pakiet_prow          char(20),
   pakiet_opis          char(255),
   primary key (pakiet_id)
);

/*==============================================================*/
/* Table: powiaty                                               */
/*==============================================================*/
create table powiaty
(
   powiat_id            char(4) not null,
   wojewodztwo_id       char(2) not null,
   powiat_nazwa         char(50) not null,
   primary key (powiat_id)
);

/*==============================================================*/
/* Table: uprawy                                                */
/*==============================================================*/
create table uprawy
(
   uprawa_id            int not null auto_increment,
   pakiet_id            smallint not null,
   gatunek_id           smallint not null,
   uprawa_symbol        char(5) not null,
   uprawa_rok 			char(10) not null,
   uprawa_uwagi         char(255),
   primary key (uprawa_id)
);

/*==============================================================*/
/* Table: wojewodztwa                                           */
/*==============================================================*/
create table wojewodztwa
(
   wojewodztwo_id       char(2) not null,
   wojewodztwo_nazwa    char(30) not null,
   primary key (wojewodztwo_id),
   unique index wojewodztwo_nazwa_UNIQUE (wojewodztwo_nazwa asc)
);

/*==============================================================*/
/* Table: zabiegi                                               */
/*==============================================================*/
create table zabiegi
(
   zabieg_id            int not null auto_increment,
   uprawa_id            int not null,
   zabieg_rodzaj_id     smallint not null,
   zabieg_data          date not null,
   zabieg_koszt_pracy   numeric(6,2),
   zabieg_koszt_paliwo  numeric(6,2),
   zabieg_koszt_materia³y numeric(6,2),
   zabieg_koszt_awaria  numeric(6,2),
   zabieg_uwagi         char(255),
   primary key (zabieg_id)
);

/*==============================================================*/
/* Table: zabiegi_rodzaje                                       */
/*==============================================================*/
create table zabiegi_rodzaje
(
   zabieg_rodzaj_id     smallint not null auto_increment,
   zabieg_rodzaj_nazwa  char(50) not null,
   primary key (zabieg_rodzaj_id),
   unique index zabieg_rodzaj_nazwa_UNIQUE (zabieg_rodzaj_nazwa asc)
);

alter table dzialki add constraint FK_obreb_dzialki foreign key (obreb_id)
      references obreby (obreb_id) on delete restrict on update restrict;

alter table dzialki_uprawy add constraint FK_dzialka_uprawy foreign key (dzialki_id)
      references dzialki (dzialki_id) on delete restrict on update restrict;

alter table dzialki_uprawy add constraint FK_uprawa_dzialki foreign key (uprawa_id)
      references uprawy (uprawa_id) on delete restrict on update restrict;

alter table gminy add constraint FK_powiat_gminy foreign key (powiat_id)
      references powiaty (powiat_id) on delete restrict on update restrict;

alter table obreby add constraint FK_gmina_obreby foreign key (gmina_id)
      references gminy (gmina_id) on delete restrict on update restrict;

alter table powiaty add constraint FK_wojewodztwo_powiaty foreign key (wojewodztwo_id)
      references wojewodztwa (wojewodztwo_id) on delete restrict on update restrict;

alter table uprawy add constraint FK_gatunek_uprawy foreign key (gatunek_id)
      references gatunki (gatunek_id) on delete restrict on update restrict;

alter table uprawy add constraint FK_pakiet_uprawy foreign key (pakiet_id)
      references pakiety (pakiet_id) on delete restrict on update restrict;

alter table zabiegi add constraint FK_rodzaj_zabiegi foreign key (zabieg_rodzaj_id)
      references zabiegi_rodzaje (zabieg_rodzaj_id) on delete restrict on update restrict;

alter table zabiegi add constraint FK_uprawa_zabiegi foreign key (uprawa_id)
      references uprawy (uprawa_id) on delete restrict on update restrict;

