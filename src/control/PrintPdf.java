package control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/**
 * Wydruk 
 * @author Igor
 * @version 1.1
 */
public class PrintPdf {
    
    /**
     * 
     * @param arrayListTab
     * @param arrayListSumm1
     * @param function
     */
    public PrintPdf( 
            ArrayList<ArrayList<String>> arrayListTab, 
            ArrayList<ArrayList<String>> arrayListSumm1, 
            int function){
        this.arrayListTab = arrayListTab;
        this.arrayListSumm1 = arrayListSumm1;
        columnWidth = 15;
        if(function == 1){
            this.file_name = "Dzialki";
            this.function = 1;
            tabSize = 10;
        } else if(function == 3){
            this.file_name = "Zabiegi";
            this.function = 3;
            tabSize = 9;
            
        }
        create();
        write();
        save();
    }
    
    /**
     * 
     * @param arrayListTab
     * @param stringSumm
     * @param arrayListSumm1
     * @param arrayListSumm2
     */
    public PrintPdf(
            ArrayList<ArrayList<String>> arrayListTab, 
            String[] stringSumm,
            ArrayList<ArrayList<String>> arrayListSumm1,
            ArrayList<ArrayList<String>> arrayListSumm2){ 
        this.file_name = "Uprawy";
        this.arrayListTab = arrayListTab;
        this.stirngSumm = stringSumm;
        this.arrayListSumm1 = arrayListSumm1;
        this.arrayListSumm2 = arrayListSumm2;
        this.function = 2;
        tabSize = 7;
        columnWidth = 15;
        create();
        write();
        save();
    }
    
    /**
     * Utworzenie nowego dokumentu
     */
    private void create(){
        document = new PDDocument();
        page = new PDPage(new PDRectangle(1050, 740));
        document.addPage(page);
        blancText = "                                                        "
                + "                                                          ";
    }
    
    /**
     * 
     * @param string
     * @return 
     */
    private String changePolishSign(String string){
        String charPL[] = {
            "Ą", "ą", "Ć", "ć", "Ę", "ę", "Ł", "ł", "Ń", "ń", 
            "Ó", "ó", "Ś", "ś", "Ź", "ź", "Ż", "ż"};
        String charEN[] = {
            "A", "a", "C", "c", "E", "e", "L", "l", "N", "n", 
            "O", "o", "S", "s", "Z", "z", "Z", "z"};
        
        for( int i = 0; i < 18; i++){
            string = string.replace(charPL[i], charEN[i]);
        }
        
        return string;
    }
    
    /**
     * 
     */
    private void writeTableHeader(){
        if(function == 1){
            try {
                contentStream.drawString("Baza dzialek rolnych");
                contentStream.moveTextPositionByAmount(0, -15);
                
                String title[] = {
                    "Województwo", "Powiat", "Gmina", "Obręb ewidencyjny", 
                    "Nr oobrębu", "Nr działki", "Pow. fizyczna", "Pow. rolna", 
                    "Pow. nie rolna", "Pow. PEG"};
                
                for (int i = 0; i < tabSize; i++) {
                        contentStream.drawString(changePolishSign(
                                ("| " + title[i] + blancText)
                                .substring(0, columnWidth)));
                        
                        contentStream.moveTextPositionByAmount(100, 0);
                }
                contentStream.drawString("|");
                contentStream.moveTextPositionByAmount(-(100 * tabSize), -20);
                
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        } else if(function == 2){
            try {
                contentStream.drawString("Baza upraw rolnych");
                contentStream.moveTextPositionByAmount(0, -15);
                
                String title[] = {
                    "Rok", "Symbol", "Nr działki", "Pow. użyta działki", 
                    "Uprawa", "Pakiet PROW", "Uwagi"};
                
                for (int i = 0; i < tabSize; i++) {
                        contentStream.drawString(changePolishSign(
                                ("| " + title[i] + blancText)
                                .substring(0, columnWidth)));
                        
                        contentStream.moveTextPositionByAmount(100, 0);
                }
                
                contentStream.moveTextPositionByAmount(300, 0);
                contentStream.drawString("|");
                contentStream
                        .moveTextPositionByAmount(-(100 * tabSize) - 300, -20);
                
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        } if(function == 3){
            try {
                contentStream.drawString("Baza zabiegów agrotechnicznych");
                contentStream.moveTextPositionByAmount(0, -15);
                
                String title[] = {
                    "Rok", "Symbol", "Gatunek", "Pakiet PROW", 
                    "Zabieg", "Data", "Koszt usług", "Koszt materiałów", 
                    "Uwagi"};
                
                for (int i = 0; i < tabSize; i++) {
                        contentStream.drawString(changePolishSign(
                                ("| " + title[i] + blancText)
                                .substring(0, columnWidth)));
                        
                        contentStream.moveTextPositionByAmount(100, 0);
                }
                contentStream.moveTextPositionByAmount(100, 0);
                contentStream.drawString("|");
                contentStream
                        .moveTextPositionByAmount(-(100 * tabSize) - 100, -20);
                
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * 
     */
    private void writeTableContents(){
        if(function == 1){
            try {
                for (int i = 0; i < arrayListTab.size(); i++) {
                    for (int j = 0; j < tabSize; j++) {
                        contentStream.drawString(changePolishSign(
                                ("| " + arrayListTab.get(i).get(j) + blancText)
                                .substring(0, columnWidth)));

                        contentStream.moveTextPositionByAmount(100, 0);

                    }
                    contentStream.drawString("|");
                    contentStream
                            .moveTextPositionByAmount(-(100 * tabSize), -10);
                }
                contentStream.moveTextPositionByAmount(0, -10);
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        } else if(function == 2){
            try {
                for (int i = 0; i < arrayListTab.size(); i++) {
                    for (int j = 0; j < tabSize; j++) {
                        if(j == 6){ charX = 60;} else {charX = 0;}
                        contentStream.drawString(changePolishSign(
                                ("| " + arrayListTab.get(i).get(j) + blancText)
                                .substring(0, columnWidth + charX)));

                        contentStream.moveTextPositionByAmount(100, 0);
                    }
                    contentStream.moveTextPositionByAmount(300, 0);
                    contentStream.drawString("|");
                    contentStream.moveTextPositionByAmount(
                            -(100 * tabSize) - 300, -10);
                }
                contentStream.moveTextPositionByAmount(0, -10);
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        } else if(function == 3){
            
            try {
                for (int i = 0; i < arrayListTab.size(); i++) {
                    for (int j = 0; j < tabSize; j++) {
                        
                        if(j == 8){ charX = 20;} else {charX = 0;}
                        contentStream.drawString(changePolishSign(
                                ("| " + arrayListTab.get(i).get(j) + blancText)
                                .substring(0, columnWidth + charX)));

                        contentStream.moveTextPositionByAmount(100, 0);

                    }
                    contentStream.moveTextPositionByAmount(100, 0);
                    contentStream.drawString("|");
                    contentStream.moveTextPositionByAmount(
                            -(100 * tabSize) - 100, -10);
                }
                contentStream.moveTextPositionByAmount(0, -10);
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * 
     */
    private void writeSummHeader(){
        if(function == 1){
            try {
                contentStream.drawString(
                        "Podsumowanie powierzchni dzialek rolnych");
                contentStream.moveTextPositionByAmount(0, -20);
                
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        } else if(function == 2){
            try {
                contentStream.drawString(
                        "Podsumowanie powierzchni upraw rolnych");
                contentStream.moveTextPositionByAmount(0, -20);
                
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        } else if(function == 3){
            try {
                contentStream.drawString(
                        "Kosztorys zabiegów agrotechnicznych");
                contentStream.moveTextPositionByAmount(0, -20);
                
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * 
     */
    private void writeSummContents(){
        if(function == 1){
            try {
                for (int i = 0; i < arrayListSumm1.size(); i++) {
                    
                    contentStream.drawString(changePolishSign(
                            arrayListSumm1.get(i).get(2)
                                    + " (" + arrayListSumm1.get(i).get(0)
                                    + "/" + arrayListSumm1.get(i).get(1)
                                    + ") Powierzchnia fizyczna: " 
                                    + arrayListSumm1.get(i).get(3)
                                    + "ha, (w tym rolna:" 
                                    + arrayListSumm1.get(i).get(4)
                                    + "ha, nie rolna: " 
                                    + arrayListSumm1.get(i).get(5)
                                    + "ha), PEG:" 
                                    + arrayListSumm1.get(i).get(6)
                                    + "ha."));

                    contentStream.moveTextPositionByAmount(0, -15);
                }
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        } else if(function == 2){
            try {
                contentStream.drawString(changePolishSign(
                        "Powierzchnia gospodarstwa"));
                contentStream.moveTextPositionByAmount(0, -20);
                contentStream.drawString(changePolishSign(
                        "Fizyczna: " 
                                + stirngSumm[0]
                                + "ha, użykowana rolniczo: "
                                + stirngSumm[1] 
                                + "ha"));
                contentStream.moveTextPositionByAmount(0, -30);
                
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
            try {
                contentStream.drawString(changePolishSign(
                        "Powierzchnia w ramach pakietów PROW"));
                contentStream.moveTextPositionByAmount(0, -20);
                for (int i = 0; i < arrayListSumm1.size(); i++) {
                    
                    contentStream.drawString(changePolishSign(
                            "PROW: " + arrayListSumm1.get(i).get(0)
                                    + ",  pakiet" 
                                    + arrayListSumm1.get(i).get(1)
                                    + ",  powierzchnia: " 
                                    + arrayListSumm1.get(i).get(2)
                                    + "ha   (" 
                                    + arrayListSumm1.get(i).get(3)
                                    + ")"));

                    contentStream.moveTextPositionByAmount(0, -15);
                }
                contentStream.moveTextPositionByAmount(0, -15);
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
            try {
                contentStream.drawString(changePolishSign(
                        "Powierzchnia w ramach gatunków"));
                contentStream.moveTextPositionByAmount(0, -20);
                for (int i = 0; i < arrayListSumm2.size(); i++) {
                    
                    contentStream.drawString(changePolishSign(
                            "Grupa: " + arrayListSumm2.get(i).get(0)
                                    + ",  gatunek: " 
                                    + arrayListSumm2.get(i).get(1)
                                    + ",  powierzchnia: " 
                                    + arrayListSumm2.get(i).get(2)
                                    + "ha"));

                    contentStream.moveTextPositionByAmount(0, -15);
                }
                contentStream.moveTextPositionByAmount(0, -15);
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        } else if(function == 3){
            float summ =0;
            try {
                for (int i = 0; i < arrayListSumm1.size(); i++) {
                    
                    contentStream.drawString(changePolishSign(
                            arrayListSumm1.get(i).get(0)
                                    + ": koszt usług - " 
                                    + arrayListSumm1.get(i).get(1)
                                    + "zł, koszt materiałów - " 
                                    + arrayListSumm1.get(i).get(2)
                                    + "zł, łączny koszt - " 
                                    + arrayListSumm1.get(i).get(3)
                                    + "zł."));
                    summ += Float.parseFloat(arrayListSumm1.get(i).get(3));

                    contentStream.moveTextPositionByAmount(0, -15);
                }
                contentStream.drawString(changePolishSign(
                        "Łączny koszt prac wynosi " + summ + "zł"));
                contentStream.moveTextPositionByAmount(0, -15);
            } catch (IOException ex) {
                Logger.getLogger(PrintPdf.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * Zapis treści
     */
    private void write(){
        try {
            contentStream = new PDPageContentStream(document, page);
            
            contentStream.beginText();
            contentStream.moveTextPositionByAmount(25, 710);
            
            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 12);
            writeTableHeader();
            contentStream.setFont(PDType1Font.HELVETICA, 10);
            writeTableContents();
            
            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 12);
            writeSummHeader();
            contentStream.setFont(PDType1Font.HELVETICA, 10);
            writeSummContents();
            
            contentStream.endText();
            contentStream.close();
        } catch (IOException ex) {
            Logger.getLogger(PrintPdf.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Zapis pliku
     */
    private void save(){
        try {
            document.save(file_name + ".pdf");
            document.close();
            JOptionPane.showMessageDialog(null, 
                    "Dokument zapisany pomyślnie.", 
                    "Info", 
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            Logger.getLogger(PrintPdf.class.getName())
                    .log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, 
                    "Błąd zapisu.", 
                    "Error", 
                    JOptionPane.ERROR_MESSAGE);
        } catch (COSVisitorException ex) {
            Logger.getLogger(PrintPdf.class.getName())
                    .log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, 
                    "Błąd zapisu.", 
                    "Error", 
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
    //Zmienne
    private PDDocument document;
    private PDPage page;
    private String file_name;
    private ArrayList<ArrayList<String>> arrayListTab;
    private String[] stirngSumm;
    private ArrayList<ArrayList<String>> arrayListSumm1;
    private ArrayList<ArrayList<String>> arrayListSumm2;
    private PDPageContentStream contentStream;
    private int function;
    private int tabSize;
    private int columnWidth;
    private String blancText;
    private int charX;
}
