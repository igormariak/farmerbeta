package control;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Komunikacja aplikacja - baza danych. Wywołanie procedur bazy danych.
 * 
 * @author Igor
 * @version 1.6
 */
public class dataBase {
    
    String url; // Adres URL bazy danych
    String username; // Nazwa użytkownika bazy danych
    String password; // Hasło użytkownika bazy danych
    Connection connection; // Połączenie z bazą danych
    ResultSet resultSet; // Obiekt danych pobranych z bazy danych
    
    /**
     * Konstruktor
     */
    private dataBase(){
        
        this.url = "jdbc:mysql://localhost:3306/farmer_beta";
        this.username = "root";
        this.password = "root";
        
        try {
            this.connection = (Connection) 
                    DriverManager.getConnection(url, username, password);
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/dataBase");
            ex.printStackTrace();
        }
    }
    
    /**
     * Utworzenie instancji singletnu klasy bazy danych
     * @return singleton
     */
    public static dataBase getInstance() {
        
        return dataHolder.INSTANCE;
    }
    
    private static class dataHolder {

        private static final dataBase INSTANCE = new dataBase();
    }
    
//    public static void main(String args[]) {
//    }
    
    /**
     * Przesłanie kwerendy do bazy i odebranie obiektu danych
     * @param string kwerenda
     * @return obiekt danych
     */
    private ResultSet getData(String string){
        
        try {
            resultSet = connection.prepareStatement(string).executeQuery();
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getData");
            ex.printStackTrace();
        }
        
        return resultSet;
    }
    
    /**
     * Pobieranie z bazy danych listy województw
     * @return Lista województw
     */
    public ArrayList<String> getProvince(){
        
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getProvince()");
            list.add("--wybierz--");
            while(resultSet.next()){
                list.add(resultSet.getString(1));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getProvince");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobieranie z bazy danych listy powiatów przynależnych do wojewdztwa 
     * @param province województwo
     * @return Lista powiatów
     */
    public ArrayList<String> getDistrict(String province){
        
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getDistrict('"
                    + province +"')");
            list.add("--wybierz--");
            while(resultSet.next()){
                list.add(resultSet.getString(1));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getDistrict");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobieranie z bazy danych listy gmin przynależnych do województwa 
     * i powiatu
     * @param province województwo
     * @param district powiat
     * @return Lista gmin
     */
    public ArrayList<String> getCommune(String province, String district){
        
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getCommune('"
                    + province + "','" + district +"')");
            list.add("--wybierz--");
            while(resultSet.next()){
                list.add(resultSet.getString(1));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getCommune");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobieranie z bazy danych listy obrębów ewidencyjnych przynależnych 
     * do województwa, powiatu i gminy
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @return Lista obrębów ewidencyjnych
     */
    public ArrayList<String> getPrecinct(String province, String district, 
            String commune){
        
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getPrecinct('"
                    + province + "','" + district + "','" + commune +"')");
            list.add("--wybierz--");
            while(resultSet.next()){
                list.add(resultSet.getString(1));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getPrecinct");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobieranie z bazy danych listy działek z pełną informacją o lokalizacji 
     * i powierzchniach
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @param precinct obręb ewidencyjny
     * @param land działka
     * @return Lista działek (województwo / powiat / gmina / obręb ewidencyny 
     * / numer obrębu / numer działki / powiarzchnia fizyczna 
     * / powierzchnia rolna / powierzchnia nierolna / powierzchnia PEG)
     */
    public ArrayList<ArrayList<String>> getLandTable(String province, 
            String district, String commune, String precinct, String land){
        
        ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getLandTable('"
                    + province + "','" + district + "','" + commune + "','" 
                    + precinct + "','" + land +"')");
            
            while(resultSet.next()){
                ArrayList<String> string = new ArrayList<String>();
                for(int i = 1; i <= 10; i++){
                    string.add(resultSet.getString(i));
                }
                list.add(string);
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getLandTable");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobranie z bazy ganych podumowania wielkości gospodarstwa 
     * w danych gminach
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @return Lista gmin wraz z powierzchniami gospodarstwa
     */
    public ArrayList<ArrayList<String>> getSummLand(String province, 
            String district, String commune){
        
        ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getSummLand('"
                    + province + "','" + district + "','" + commune +"')");
            
            while(resultSet.next()){
                ArrayList<String> string = new ArrayList<String>();
                for(int i = 1; i <= 7; i++){
                    string.add(resultSet.getString(i));
                }
                list.add(string);
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getSummLand");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Sprawdzenie czy istnieje działka o podanym numerze z podanej lokalizacji
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @param precinct obręb ewidencyjny
     * @param land numer działki
     * @return Odpowiedź czy działka istnieje.
     */
    public boolean checkNumber(String province, String district, String commune, 
            String precinct, String land){
        
        boolean result = true;
        
        try {
            ResultSet resultSet = getData("call farmer_beta.checkNumber('"
                    + province + "','" + district + "','" + commune + "','" 
                    + precinct + "','" + land +"')");
            resultSet.next();
            if(resultSet.getInt(1) == 0){
                result = false;
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/checkNumber");
            ex.printStackTrace();
        }
        
        return result;
    }
    
    /**
     * Dodanie rekordu bazy danych - nowa działka ewidencyjna
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @param precinct obręb ewidencyjny
     * @param land numer działki
     * @param surface1 powierzchnia rolna (grunty orne, trwałe użytki zielone 
     * i rowy do 2 metrów szerokości)
     * @param surface2 powierzchnia nierolna (nieużytki, lasy, rowy powyżej 
     * 2 metrów szerokości)
     * @param surface3 powierzchnia PEG
     */
    public void addLand(String province, String district, String commune, 
            String precinct, String land, String surface1, String surface2, 
            String surface3){
        
        try {
            //Pobranie ID obrębu ewidencyjnego
            ResultSet resultSet = getData("call farmer_beta.getPrecinctId('"
                    + province + "','" + district + "','" + commune 
                    + "','" + precinct +"')");
            resultSet.next();
            String precinctId = resultSet.getString(1);
            //Dodanie rekordu
            getData("call farmer_beta.addLand('"+ precinctId + "','" 
                    + land + "','" + surface1 + "','" + surface2 
                    + "','" + surface3 +"')");
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/addLand");
            ex.printStackTrace();
        }
    }
    
    /**
     * Pobieranie z bazy danych listy działek ewidencyjnych przynależnych 
     * do województwa, powiatu, gminy i obrębu ewidencyjnego
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @param precinct obręb ewidencyjny
     * @return Lista działek ewidencyjnych
     */
    public ArrayList<String> getLand(String province, String district, 
            String commune, String precinct){
        
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getLand('"
                    + province + "','" + district + "','" + commune 
                    + "','" + precinct +"')");
            list.add("--wybierz--");
            while(resultSet.next()){
                list.add(resultSet.getString(1));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getLand");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobieranie z bazy danych powierzchni działki ewidencyjnej
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @param precinct obręb ewidencyjny
     * @param land działka
     * @return Powierzchnie działki : rolna, nierolna, PEG
     */
    public String[] getSurface(String province, String district, String commune, 
            String precinct, String land){
        
        String[] string = {"","",""};
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getSurface('"
                    + province + "','" + district + "','" + commune + "','" 
                    + precinct + "','" + land +"')");
            resultSet.next();
            string[0] = resultSet.getString(1);
            string[1] = resultSet.getString(2);
            string[2] = resultSet.getString(3);
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getSurface");
            ex.printStackTrace();
        }
        
        return string;
    }
    
    /**
     * Pobranie z bazy danych identyfikatora rekordu działki
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @param precinct obręb ewidencyjny
     * @param land działka
     * @return Numer ID działki
     */
    public String getLandId(String province, String district, String commune, 
            String precinct, String land){
        
        String result = "";
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getLandId('"
                    + province + "','" + district + "','" + commune + "','" 
                    + precinct + "','" + land +"')");
            resultSet.next();
            result = resultSet.getString(1);
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getLandId");
            ex.printStackTrace();
        }
        
        return result;
    }
    
    /**
     * Edycja rekordu działki w bazie
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @param precinct obręb ewidencyjny
     * @param land działka
     * @param surface1 powierzchnia rolna
     * @param surface2 powierzchnia nierolna
     * @param surface3 powierzchnia PEG
     */
    public void editLand(String province, String district, String commune, 
            String precinct, String land, String surface1, 
            String surface2, String surface3){
        
        try {
            // Pobranie ID
            String landId = getLandId(province, district, commune, 
                    precinct, land);
            //Edycja rekordu
            getData("call farmer_beta.editLand('"+ landId + "','" + surface1 
                    + "','" + surface2 + "','" + surface3 +"')");
        } catch (NullPointerException ex) {
            System.err.println("dataBase/editLand");
            ex.printStackTrace();
        }
    }
    
    /**
     * Usunięcie działki z bazy danych
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @param precinct obręb ewidencyjny
     * @param land działka
     */
    public void removeLand(String province, String district, String commune, 
            String precinct, String land){
        
        try {
            // Pobranie ID
            String landId = getLandId(province, district, commune, 
                    precinct, land);
            //Usunięcie rekordu
            getData("call farmer_beta.removeLand('"+ landId +"')");
        } catch (NullPointerException ex) {
            System.err.println("dataBase/removeLand");
            ex.printStackTrace();
        }
    }
    
    /**
     * Pobranie listy grup gatunków upraw
     * @return Lista grup
     */
    public ArrayList<String> getSpeciesGroup(){
        
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getSpeciesGroup()");
            list.add("--wybierz--");
            while(resultSet.next()){
                list.add(resultSet.getString(1));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getSpeciesGroup");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobranie listy gatunków upraw z podanej grupy
     * @param group grupa upraw
     * @return Lista upraw
     */
    public ArrayList<String> getSpecies(String group){
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getSpecies('" 
                    + group + "')");
            list.add("--wybierz--");
            while(resultSet.next()){
                list.add(resultSet.getString(1));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getSpecies");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Sprawdzenie czy w danym roku nie występuje już podany symbol
     * @param jear rok
     * @param symbol symbol
     * @return Czy istnieje symbol
     */
    public boolean checkSymbol(String jear, String symbol){
        
        boolean result = true;
        
        try {
            ResultSet resultSet = getData("call farmer_beta.checkSymbol('"
                    + jear + "','" + symbol +"')");
            resultSet.next();
            if(resultSet.getInt(1) == 0){
                result = false;
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/checkNumber");
            ex.printStackTrace();
        }
        
        return result;
    }
    
    /**
     * Pabranie listy pakietów podanego PROWu
     * @param prow lata PROW
     * @return Lista pakietów
     */
    public ArrayList<String> getPackages(String prow){
        
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getPackages('" 
                    + prow + "')");
            list.add("--wybierz--");
            while(resultSet.next()){
                list.add(resultSet.getString(1));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getPackages");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobranie informacji wybranego pakietu
     * @param prow lata PROW
     * @param packages pakiet
     * @return 
     */
    public String getPackagesInfo(String prow, String packages){
        
        String string = "";
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getPackagesInfo('" 
                    + prow + "','" + packages + "')");
            resultSet.next();
            string = resultSet.getString(1);
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getPackagesInfo");
            ex.printStackTrace();
        }
        
        return string;
    }
    
    /**
     * Pobranie listy sybloli upraw wg kryteriów (% - dowolny rekord)
     * @param jear rok
     * @param prow pakiet prow
     * @param packages nazwa pakietu prow
     * @param group grupa gatunków
     * @param species gatunki
     * @return Lista symboli
     */
    public ArrayList<String> getSymbols(String jear, String prow, String packages, 
            String group, String species){
        
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getSymbols('" 
                    + jear + "','" + prow + "','" + packages + "','" 
                    + group+ "','" + species + "')");
            list.add("--wybierz--");
            while(resultSet.next()){
                list.add(resultSet.getString(1));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getSymbols");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobiera informacje o uprawie
     * @param jear rok
     * @param symbol symbol
     * @return Informacje o uprawie : 
     * grupa gatunku / gatunek / pakiet prow 
     * / nazwa pakietu prow / uwagi do uprawy
     */
    public ArrayList<String> getCultivInfo(String jear, String symbol){
        
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getCultivInfo('" 
                    + jear + "','" + symbol + "')");
            resultSet.next();
            for(int i = 1; i < 6; i++){
                list.add(resultSet.getString(i));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getCultivInfo");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobranie identyfikatora pakietu prow
     * @param prow pakiet prow
     * @param packages nazwa pakietu prow
     * @return Numer ID pakietu prow
     */
    public String getPackageId(String prow, String packages){
        
        String string = "";
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getPackageId('" 
                    + prow + "','" + packages + "')");
            resultSet.next();
            string = resultSet.getString(1);
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getPackageId");
            ex.printStackTrace();
        }
        
        return string;
    }
    
    /**
     * Pobranie identyfikatora gatunku uprawy
     * @param group grupa upraw
     * @param species nawzwa gatunku
     * @return Numer ID gatunku
     */
    public String getSpeciesId(String group, String species){
        
        String string = "";
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getSpeciesId('" 
                    + group + "','" + species + "')");
            resultSet.next();
            string = resultSet.getString(1);
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getSpeciesId");
            ex.printStackTrace();
        }
        
        return string;
    }
    
    /**
     * Dodanie rekordu upraw do bazy danych
     * @param prow pakiet prow
     * @param packages nazwa pakietu prow
     * @param group grupa upraw
     * @param species nazwa gatunku
     * @param symbol symbol uprawy
     * @param jear rok uprawy
     * @param info uwagi i dodatkowe informacje
     */
    public void addCultiv(String prow, String packages, String group, 
            String species, String symbol, String jear, String info){
        
        try {
            //Pobranie ID pakietu prow
            String packageId = getPackageId(prow, packages);
            //Pobranie ID gatunku
            String speciesId = getSpeciesId(group, species);
            //Dodanie rekordu
            getData("call farmer_beta.addCultiv('"+ packageId + "','" 
                    + speciesId + "','" + symbol + "','" + jear 
                    + "','" + info +"')");
        } catch (NullPointerException ex) {
            System.err.println("dataBase/addCultiv");
            ex.printStackTrace();
        }
    }
    
    /**
     * Pobranie identyfiaktora uprawy
     * @param jear rok uprawy
     * @param symbol symbol uprawy
     * @return Numer ID uprawy
     */
    public String getCultivId(String jear, String symbol){
        
        String string = "";
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getCultivId('" 
                    + jear + "','" + symbol + "')");
            resultSet.next();
            string = resultSet.getString(1);
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getCultivId");
            ex.printStackTrace();
        }
        
        return string;
    }
    
    /**
     * Edycja rekordu uprawy w bazie danych
     * @param prow pakiet prow
     * @param packages nazwa pakietu
     * @param group grupa upraw
     * @param species nazwa gatunku
     * @param symbol symbol uprawy
     * @param jear rok uprawy
     * @param info uwagi do uprawy
     */
    public void editCultiv(String prow, String packages, String group, 
            String species, String symbol, String jear, String info){
        
        try {
            //Pobranie ID uprawy
            String cultivId = getCultivId(jear, symbol);
            //Pobranie ID pakietu prow
            String packageId = getPackageId(prow, packages);
            //Pobranie ID gatunku
            String speciesId = getSpeciesId(group, species);
            //Edycja rekordu
            getData("call farmer_beta.editCultiv('"+ packageId + "','" 
                    + speciesId + "','" + cultivId + "','" + info +"')");
        } catch (NullPointerException ex) {
            System.err.println("dataBase/editCultiv");
            ex.printStackTrace();
        }
    }
    
    /**
     * Usunięcie rekordu uprawy z bazy danych
     * @param jear rok uprawy
     * @param symbol symbol uprawy
     */
    public void removeCultiv(String jear, String symbol){
        
        try {
            //Pobranie ID uprawy
            String cultivId = getCultivId(jear, symbol);
            //Usunięcie rekordu
            getData("call farmer_beta.removeCultiv('" + cultivId +"')");
        } catch (NullPointerException ex) {
            System.err.println("dataBase/removeCultiv");
            ex.printStackTrace();
        }
    }
    
    /**
     * Dodanie rekordu bazy danych - nowe połączenie uprawy i działki
     * @param jear rok
     * @param symbol symbol
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @param precinct obręb
     * @param land działka
     * @param surface powierzchnia
     */
    public void addConnect(String jear, String symbol, String province, 
            String district, String commune, String precinct, 
            String land, String surface){
        
        try {
            String cultivId = getCultivId(jear, symbol);
            String landId = 
                    getLandId(province, district, commune, precinct, land);
            
            //Dodanie rekordu
            getData("call farmer_beta.addConnect('"+ cultivId + "','" 
                    + landId + "','" + surface +"')");
        } catch (NullPointerException ex) {
            System.err.println("dataBase/addConnect");
            ex.printStackTrace();
        }
    }
    
    /**
     * Edycja rekordu bazy danych - modyfikacja połączenia działki i uprawy
     * @param jear rok
     * @param symbol symbol
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @param precinct obręb
     * @param land działka
     * @param surface powierzchnia
     */
    public void editConnect(String jear, String symbol, String province, 
            String district, String commune, String precinct, 
            String land, String surface){
        
        try {
            String cultivId = getCultivId(jear, symbol);
            String landId = 
                    getLandId(province, district, commune, precinct, land);
            
            //Dodanie rekordu
            getData("call farmer_beta.editConnect('"+ cultivId + "','" 
                    + landId + "','" + surface +"')");
        } catch (NullPointerException ex) {
            System.err.println("dataBase/editConnect");
            ex.printStackTrace();
        }
    }
    
    /**
     * Usunięcie rekordu bazy danych - usunięcie połączenia działki i uprawy
     * @param jear rok
     * @param symbol symbol
     * @param province województwo
     * @param district powiat
     * @param commune gmina
     * @param precinct obręb
     * @param land działka
     */
    public void removeConnect(String jear, String symbol, String province, 
            String district, String commune, String precinct, 
            String land){
        
        try {
            String cultivId = getCultivId(jear, symbol);
            String landId = 
                    getLandId(province, district, commune, precinct, land);
            
            //Dodanie rekordu
            getData("call farmer_beta.removeConnect('"+ cultivId + "','" 
                    + landId +"')");
        } catch (NullPointerException ex) {
            System.err.println("dataBase/removeConnect");
            ex.printStackTrace();
        }
    }
    
    /**
     * Pobranie listy działek podpiętych do symbolu uprawy
     * @param jear rok
     * @param symbol symbol
     * @return Lista działek
     */
    public ArrayList<String> getCultivLand(String jear, String symbol){
        
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            String cultivId = getCultivId(jear, symbol);
            
            ResultSet resultSet = getData("call farmer_beta.getCultivLand('" 
                    + cultivId + "')");
            list.add("--wybierz--");
            while(resultSet.next()){
                list.add(resultSet.getString(1));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getSymbols");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobranie informacji o podpiętej działce
     * @param jear rok
     * @param symbol symbol
     * @param number numer
     * @return Lokalizacja województwo / powiat / gmina / obręb
     */
    public String[] getCultivLandInfo(
            String jear, String symbol, String number){
        
        String string[] = new String[5];
        
        try {
            String cultivId = getCultivId(jear, symbol);
            
            ResultSet resultSet = getData("call farmer_beta.getCultivLandInfo('" 
                    + cultivId + "','"+ number + "')");
            resultSet.next();
            for(int i = 1 ; i < 6; i++){
                string[i-1] = resultSet.getString(i);
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getSymbols");
            ex.printStackTrace();
        }
        
        return string;
    }
    
    /**
     * Baza upraw i podpiętych działek
     * @param jear rok
     * @param group grupa
     * @param species gatunek
     * @param prow prow
     * @param packages pakiet
     * @return  Lista upraw, podpiętych działek
     */
    public ArrayList<ArrayList<String>> getCultivTable(String jear, 
            String group, String species, String prow, String packages){
        
        ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getCultivTable('"
                    + jear + "','" + group + "','" + species + "','" 
                    + prow + "','" + packages +"')");
            
            while(resultSet.next()){
                ArrayList<String> string = new ArrayList<String>();
                for(int i = 1; i <= 7; i++){
                    string.add(resultSet.getString(i));
                }
                list.add(string);
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getCultivTable");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Podsumowanie uprawianego gospodarstwa wg powierzchni 
     * fizycznej i użytkowej
     * @param jear rok
     * @return Powierzchnia fizyczna i użytkowa
     */
    public String[] getCultivInfoLand(String jear){
        
        String string [] = new String[2];
        try {
            ResultSet resultSet = getData("call farmer_beta.getCultivInfoLand('"
                    + jear +"')");
            resultSet.next();
            string[0] = resultSet.getString(1);
            string[1] = resultSet.getString(2);
             
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getCultivInfoLand");
            ex.printStackTrace();
        }
        
        return string;
    }
    
    /**
     * Podsumowanie powierzchni wg pakietów prow
     * @param jear rok
     * @param group grupa
     * @param species gatunek
     * @param prow prow
     * @param packages pakiet
     * @return Lista pakietów i powierzchni
     */
    public ArrayList<ArrayList<String>> getCultivInfoProw(String jear, 
            String group, String species, String prow, String packages){
        
        ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getCultivInfoProw('"
                    + jear + "','" + group + "','" + species + "','" 
                    + prow + "','" + packages +"')");
            
            while(resultSet.next()){
                ArrayList<String> string = new ArrayList<String>();
                for(int i = 1; i <= 4; i++){
                    string.add(resultSet.getString(i));
                }
                list.add(string);
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getCultivInfoProw");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Podsumowanie powierzchni wg gatunku uprawy
     * @param jear rok
     * @param group grupa
     * @param species gatunek
     * @param prow prow
     * @param packages pakiet
     * @return Lista gatunków powierzchni
     */
    public ArrayList<ArrayList<String>> getCultivInfoSpecies(String jear, 
            String group, String species, String prow, String packages){
        
        ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getCultivInfoSpecies('"
                    + jear + "','" + group + "','" + species + "','" 
                    + prow + "','" + packages +"')");
            
            while(resultSet.next()){
                ArrayList<String> string = new ArrayList<String>();
                for(int i = 1; i <= 3; i++){
                    string.add(resultSet.getString(i));
                }
                list.add(string);
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getCultivInfoProw");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobranie identyfikatora rodzaju zabiegu agrotechnicznego
     * @param activ nazwa rodzaju zabiegu agrotechnicznego
     * @return Identyfikator rodzaju zabiegu agrotechnicznego
     */
    public String getActivTypeId(String activ){
        
        String result = "";
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getActivTypeId('"
                    + activ +"')");
            resultSet.next();
            result = resultSet.getString(1);
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getActivTypeId");
            ex.printStackTrace();
        }
        
        return result;
    }
    
    /**
     * Dodanie rekordu bazy dazy zabiegów agrotechnicznych
     * @param jear rok uprawy
     * @param symbol symbol uprawy
     * @param activ identyfikator rodzaju zabiegu agrotechnicznego
     * @param date planowana data zabiegu
     * @param price1 wartość usług obcych
     * @param price2 wartość zużytych materiałów
     * @param info uwagi do uprawy
     */
    public void addActiv(String jear, String symbol, String activ, 
            String date, String price1, String price2, 
            String info){
        System.out.print(date);
        
        try {
            String cultivId = getCultivId(jear, symbol);
            String activId = getActivTypeId(activ);
            
            //Dodanie rekordu
            getData("call farmer_beta.addActiv('"+ cultivId + "','" 
                    + activId + "','" + date + "','" + price1 + "','" 
                    + price2 + "','" + info +"')");
        } catch (NullPointerException ex) {
            System.err.println("dataBase/addActiv");
            ex.printStackTrace();
        }
    }
    
    /**
     * Pobranie bazy zabiegów agrotechnicznych odfiltrowana parametrami
     * @param jear rok
     * @param group grupa upraw
     * @param species gatunek
     * @param activ zabieg agrotechniczny
     * @param symbol symbol uprawy
     * @return Baza zabiegów agrotechnicznych
     */
    public ArrayList<ArrayList<String>> getActivTable(String jear, 
            String group, String species, String activ, String symbol){
        
        ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getActivTable('"
                    + jear + "','" + group + "','" + species + "','" 
                    + activ + "','" + symbol +"')");
            
            while(resultSet.next()){
                ArrayList<String> string = new ArrayList<String>();
                for(int i = 1; i <= 9; i++){
                    string.add(resultSet.getString(i));
                }
                list.add(string);
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getActivTable");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobranie kosztorysu upraw
     * @param jear rok uprawy
     * @param group grupa gatunków upraw
     * @param species gatunek uprawy
     * @param activ nazwa rodzaju zabiegu agrotechnicznego
     * @param symbol symbol uprawy
     * @return Baza kosztów
     */
    public ArrayList<ArrayList<String>> getSummActiv(String jear, 
            String group, String species, String activ, String symbol){
        
        ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getSummActiv('"
                    + jear + "','" + group + "','" + species + "','" 
                    + activ + "','" + symbol +"')");
            
            while(resultSet.next()){
                ArrayList<String> string = new ArrayList<String>();
                for(int i = 1; i <= 4; i++){
                    string.add(resultSet.getString(i));
                }
                list.add(string);
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getSummActiv");
            ex.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Pobranie listy rdzajów zabiegów agrotechnicznych
     * @return  Lista rodzajów zabiegów agrotechnicznych
     */
    public ArrayList<String> getActivType(){
        
        ArrayList<String> list = new ArrayList<String>();
        
        try {
            ResultSet resultSet = getData("call farmer_beta.getActivType()");
            list.add("--wybierz--");
            while(resultSet.next()){
                list.add(resultSet.getString(1));
            }
        } catch (SQLException | NullPointerException ex) {
            System.err.println("dataBase/getActivType");
            ex.printStackTrace();
        }
        
        return list;
    }
}

    